#include <stdlib.h>
#include <stdio.h>

#include <pthread.h>
#include <fcntl.h>
#include <semaphore.h>

volatile char car_presence;
volatile int G;

enum states {
	State0,
	State1,
	State2,
	State3,
	State4,
	State5,
	State6
};

void new_car(){
	int done = 0;

	while (1){
//		printf("loopin\n");
		if (G){
			printf("\nHas a car arrived at intersection?\n enter 'e' for EW or 'n' for NS.\n");
			scanf(" %c", &car_presence);
			getchar();
		}
	}
}


void SingleStep_TrafficLight_SM(currState){
	int done = 0;
	char input;

	while (1){

		switch (currState){
			case State0:
				printf("EWR - NSR (0)\n");
				sleep(1);
//				CurState = State1;

			case State1:
				printf("EWR - NSR (1)\n");
				sleep(1);
//				break;

			case State2:
				printf("EWG - NSR (2)\n");
				G = 1;
				if (car_presence == 'n'){
					currState = 3;
				}
				else if (car_presence != 'n'){
					currState = 2;
					sleep(2);
					break;
				}

				sleep(2);

			case State3:
				G = 0;
				printf("EWY - NSR (3)\n");
				sleep(1);

			case State4:
				G = 0;
				printf("EWR - NSR (4) \n");
				sleep(1);

			case State5:
				G = 1;

				printf("EWR - NSG (5)\n");
				if (car_presence == 'e'){
					currState = 6;
				}
				else if (car_presence != 'e'){
					currState = 5;
					sleep(2);
					break;
				}

				sleep(2);

			case State6:
				printf("EWR - NSY (6)\n");
				currState = 1;
				sleep(1);
				break;
//				printf("Here we are");

			default:
				printf("Error!\n");
				break;
		}
	}

//	printf("Shouldnt see me.\n");
}

int main(int argc, char *argv[])
{
	pthread_t sensor, traffic_light;
	void * retval;

	printf("Fixed Sequence Traffic Lights State Machine Example\n");
	int Runtimes=30, counter = 0;
	enum states CurrentState = State0;


	// Declaring the enum within the main
	// means we will need to pass it by address
//	printf("%d\n", CurrentState);
//	while (counter < Runtimes)
//	{
//		SingleStep_TrafficLight_SM( CurrentState ); // pass address
//		counter++;
//	}

	pthread_create(&traffic_light, NULL, SingleStep_TrafficLight_SM, NULL);
	pthread_create(&sensor, NULL, new_car, NULL);

	pthread_join(traffic_light, &retval);
	printf("%d\n", retval);

	pthread_join(sensor, &retval);
	printf("%d\n", retval);

	printf("main terminating..\n");

}

