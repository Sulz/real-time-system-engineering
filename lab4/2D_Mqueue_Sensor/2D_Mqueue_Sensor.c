#include <stdlib.h>
#include <stdio.h>

#include <pthread.h>
#include <fcntl.h>
#include <semaphore.h>

#include <mqueue.h>
#include <sys/stat.h>

#define  MESSAGESIZE 2

#define Q_FLAGS O_RDWR | O_CREAT
#define Q_Mode S_IRUSR | S_IWUSR


void main(){

//	char car_data;
//
//
//	mqd_t mqueue;
//	struct mq_attr attr;
//
//	int i;
//	char buf;
//
//	const char * mqueue_dir = "/msg_queue";
//
//	mqueue = mq_open(mqueue_dir, O_RDONLY);
//	mq_getattr(mqueue, &attr);
//	printf ("max. %ld msgs, %ld bytes; waiting: %ld\n", attr.mq_maxmsg, attr.mq_msgsize, attr.mq_curmsgs);


	char car_data;
	mqd_t mqueue;
	char i;
	char buf;

	struct mq_attr mqueue_attributes;
	mqueue_attributes.mq_maxmsg = 1;
	mqueue_attributes.mq_msgsize = MESSAGESIZE;
	mqueue_attributes.mq_flags = 0;

	const char * mqueue_dir = "/msg_queue";

	struct	mq_attr	attr;
	mqueue = mq_open(mqueue_dir, Q_FLAGS, Q_Mode, &mqueue_attributes);

	if (mqueue != -1){
		while (1){
			printf("\nPress key for car to arrive at intersection.\n 'e' for EW, 'n' for NS.\n");
			scanf(" %c", &buf);
			getchar();

//				printf("i contains: %c\n", i);
//				sprintf(buf, &i);
			// printf("buff contains: %c\n", buf);

			mq_send(mqueue, buf, MESSAGESIZE, 0);
			mq_getattr(mqueue, &attr);
			printf ("max. %ld msgs, %ld bytes; waiting: %ld\n", attr.mq_maxmsg, attr.mq_msgsize, attr.mq_curmsgs);

			// printf("sent.\n");
			// sleep(1);
		}
	}

}
