#include <stdlib.h>
#include <stdio.h>

#include <stdlib.h>
#include <stdio.h>

enum states {
	State0,
	State1,
	State2,
	State3,
	State4,
	State5,
	State6
};


int main(int argc, char *argv[])
{
	printf("Fixed Sequence Traffic Lights State Machine Example\n");
	int Runtimes=30, counter = 0;
	enum states CurrentState = State0;
	// Declaring the enum within the main
	// means we will need to pass it by address
	printf("%d\n", CurrentState);
	while (counter < Runtimes)
	{
		SingleStep_TrafficLight_SM( CurrentState ); // pass address
		counter++;
	}
}

void SingleStep_TrafficLight_SM(currState){
	int done = 0;
	char input;

	while (!done){

		switch (currState){
			case State0:
				printf("EWR - NSR (0)\n");
				sleep(1);
//				CurState = State1;

			case State1:
				printf("EWR - NSR (1)\n");
				sleep(1);
//				break;

			case State2:
				printf("EWG - NSR (2)\n");

				printf("\nHas a car arrived at intersection?\n enter 'e' for EW or 'n' for NS.\n");
				scanf(" %c", &input);
				getchar();

				if (input == 'n'){
					currState = 3;
				}
				else if (input != 'n'){
					currState = 2;
					sleep(2);
					break;
				}

				sleep(2);

			case State3:
				printf("EWY - NSR (3)\n");
				sleep(1);

			case State4:
				printf("EWR - NSR (4) \n");
				sleep(1);

			case State5:
				printf("EWR - NSG (5)\n");

				printf("\nHas a car arrived at intersection?\n enter 'e' for EW or 'n' for NS.\n");
				scanf(" %c", &input);

				getchar();

				if (input == 'e'){
					currState = 6;
				}
				else if (input != 'e'){
					currState = 5;
					sleep(2);
					break;
				}

				sleep(2);

			case State6:
				printf("EWR - NSY (6)\n");
				currState = 1;
				sleep(1);
		}
	}
}
