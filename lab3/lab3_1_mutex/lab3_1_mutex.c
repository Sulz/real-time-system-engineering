#include <stdio.h>
#include <pthread.h>

 typedef struct
 {
	 int a;
	 int b;

	 int result;
	 int result2;

	 int use_count;
	 int use_count2;

	 int max_use;
	 int max_use2;

	 pthread_mutex_t mutex;
 } app_data;


void *user_thread(void *data)
{

	int uses=0;

	app_data *td=(app_data*)data;

	while(uses<td->max_use)
	 {
		pthread_mutex_lock(&td->mutex);
		 if (td->a==5)
		 {
			 td->result+=(td->a+td->b);
			 td->use_count++;
			 uses++;
		 }
		 pthread_mutex_unlock(&td->mutex);
		 usleep(1);
	 }
	 return 0;

}

void *changer_thread(void *data)
{
	 app_data *td=(app_data*)data;
	 while ((td->use_count+td->use_count2)<(td->max_use+td->max_use2))
	 {
		 pthread_mutex_lock(&td->mutex);
		 if (td->a==5)
		 {
			 td->a=50;
			 td->b=td->a+usleep(1000);
		 }
		 else
		 {
			 td->a=5;
			 td->b=td->a+usleep(1000);
		 }
		 pthread_mutex_unlock(&td->mutex);
		 usleep(1);
	 }
	 return 0;
}

void * subtracter_thread(void* data)
{
	int use = 0;
	app_data *td = (app_data*) data;

	while (use<td->max_use2)
	{
		 pthread_mutex_lock(&td->mutex);
		 if (td->a == 50)
		 {
			 td->result2 -= (td->a + td->b);
			 use++;
			 td->use_count2++;
		 }
		 pthread_mutex_unlock(&td->mutex);
		 usleep(1);
	}

	return 0;
}

main()
{
	pthread_t ct,ut,st;
	app_data td={5,5,0,0,0,0,100,0};

	void *retval;


	pthread_mutex_init(&td.mutex, NULL);

	pthread_create(&ut,NULL,user_thread,&td);
	pthread_create(&ct,NULL,changer_thread,&td);
	pthread_create(&st,NULL,subtracter_thread,&td);

	pthread_join(st, &retval);
	pthread_join(ct,&retval);
	pthread_join(ut,&retval);

	pthread_mutex_destroy(&td.mutex);


	printf("result should be %d, is %d\n",td.max_use*(5+5),td.result);
	printf("Result2 should be %d, is %d\n", -(td.max_use2*(50+50)), td.result2);

  }
