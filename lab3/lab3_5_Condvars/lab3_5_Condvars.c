/*
 * cp1.c
 *
 * Very simple producer-consumer example
 * showing use of condition variables
 *
*/

#include <stdio.h>
#include <pthread.h>

int data_ready = 0;
// initialize mutex and condvar with the default attributes
// (can only do this with statically allocated variables)
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t  condvar = PTHREAD_COND_INITIALIZER;

void *consumer(void *notused)
{
    printf("consumer thread started...\n");
    while(1)
    {
       pthread_mutex_lock(&mutex);
        // test the condition and wait until it is true
        while(!data_ready)
        {
            pthread_cond_wait(&condvar, &mutex);
        }
        // process data
        printf ("consumer:  got data from producer\n");

        // now change the condition and signal that it has changed
        data_ready = 0;
        pthread_cond_signal(&condvar);

		pthread_mutex_unlock(&mutex);
    }
}

void *producer(void *notused)
{
    printf("producer thread started...\n");
    while(1)
    {
        // get data from hardware
        // we'll simulate this with a sleep (1)
        sleep(1);
        printf("producer:  Data from simulated h/w ready\n");
        pthread_mutex_lock(&mutex);

        // test the condition and wait until it is true
        while(data_ready)
        {
            pthread_cond_wait(&condvar, &mutex); // This function blocks the calling thread
            									  // on the condition variable cond, and unlocks
            									  // the associated mutex.
        }

        // now change the condition and signal that it has changed
        data_ready = 1;
        pthread_cond_signal(&condvar);

		pthread_mutex_unlock(&mutex);
    }
}

int main(void)
{
   	printf("Welcome to the QNX Momentics Conditional Variables Example\n");
	printf("Starting consumer/producer example...\n");

	pthread_t  th1, th2;

	// create the producer and consumer threads
    pthread_create (&th1, NULL, producer, NULL);
    pthread_create (&th2, NULL, consumer, NULL);

    // let the threads run for a bit
    sleep (20);

	printf("\nMain Terminating....");
    return 0;
}
