#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>

enum states {
		State0, State1, State2, State3, State4, State5, State6, State7, State8, State9,
		State10, State11, State12, State13, State14, State15, State16, State17, State18, State19
};

const char * lights[] = { "N:   ","S:   ", "E:   ","W:   ","EN:  ", "WN:  ", "NSp: ", "EWp: "};
//State codes represent the following lights in order: N S E W ES WS NSPed EWPed
// Light states in order from 0 to 4 are red, yellow, green, flashing red and off

enum light_config{
	RED, YELLOW, GREEN, FL_RED, OFF
};

// Peak hours
int peak_StateCodes[24][8] = {
		{RED, RED, RED, RED, RED, RED, RED, RED},
		{RED, RED, GREEN, GREEN, OFF, OFF, RED, GREEN},
		{RED, RED, GREEN, GREEN, OFF, OFF, RED, FL_RED},
		{RED, RED, GREEN, GREEN, OFF, OFF, RED, RED},
		{RED, RED, YELLOW, YELLOW, OFF, OFF, RED, RED},
		{RED, RED, GREEN, GREEN, YELLOW, YELLOW, RED, GREEN},
		{RED, RED, GREEN, GREEN, YELLOW, YELLOW, RED, FL_RED},
		{RED, RED, GREEN, GREEN, YELLOW, YELLOW, RED, RED},
		{RED, RED, GREEN, GREEN, RED, RED, RED, GREEN},
		{RED, RED, GREEN, GREEN, RED, RED, RED, FL_RED},
		{RED, RED, GREEN, GREEN, RED, RED, RED, RED},
		{RED, RED, YELLOW, YELLOW, RED, RED, RED, RED},
		{RED, RED, RED, RED, RED, RED, RED, RED},
		{GREEN, GREEN, RED, RED, RED, RED, GREEN, RED},
		{GREEN, GREEN, RED, RED, RED, RED, FL_RED, RED},
		{GREEN, GREEN, RED, RED, RED, RED, RED, RED},
		{YELLOW, YELLOW, RED, RED, RED, RED, RED, RED},
		{YELLOW, GREEN, RED, RED, RED, RED, GREEN, RED},
		{YELLOW, GREEN, RED, RED, RED, RED, FL_RED, RED},
		{YELLOW, GREEN, RED, RED, RED, RED, RED, RED},
		{RED, GREEN, RED, RED, RED, RED, GREEN, RED},
		{RED, GREEN, RED, RED, RED, RED, FL_RED, RED},
		{RED, GREEN, RED, RED, RED, RED, RED, RED},
		{RED, YELLOW, RED, RED, RED, RED, RED, RED}};

int off_pk_StateCodes[20][8] = {
		{RED, RED, RED, RED, RED, RED, RED, RED},
		{RED, RED, GREEN, GREEN, OFF, OFF, RED, GREEN},
		{RED, RED, GREEN, GREEN, OFF, OFF, RED, FL_RED},
		{RED, RED, GREEN, GREEN, OFF, OFF, RED, RED},
		{RED, RED, YELLOW, YELLOW, YELLOW, YELLOW, RED, FL_RED},
		{RED, RED, RED, RED, RED, RED, RED, RED},
		{RED, RED, GREEN, GREEN, YELLOW, YELLOW, RED, GREEN},
		{RED, RED, GREEN, GREEN, YELLOW, YELLOW, RED, FL_RED}, //7
		{RED, RED, GREEN, GREEN, RED, YELLOW, YELLOW, GREEN},
		{RED, RED, GREEN, GREEN, RED, RED, RED, GREEN},
		{RED, RED, GREEN, GREEN, RED, RED, RED, FL_RED},
		{RED, RED, GREEN, GREEN, RED, RED, RED, RED},
		{RED, RED, YELLOW, YELLOW, RED, RED, RED, FL_RED},
		{GREEN, GREEN, RED, RED, RED, RED, GREEN, RED}, //13
		{YELLOW, GREEN, RED, RED, RED, RED, GREEN, RED},
		{RED, GREEN, RED, RED, RED, RED, GREEN, RED},
		{RED, GREEN, RED, RED, RED, RED, FL_RED, RED},
		{RED, YELLOW, RED, RED, RED, RED, RED, RED},
		{GREEN, GREEN, RED, RED, RED, RED, FL_RED, RED}, //18
		{YELLOW, YELLOW, RED, RED, RED, RED, RED, RED}};


int counter;

volatile char data = 0; //Data
volatile int data_ready = 0; // Flag

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void *producer_ex(void *notused) {
	printf("Producer Started....\n");
	char key = 'c';
	while (1) {
		sleep(3);
		if(key == 'c') key = 'n';
		else if(key == 'n') key = 't';
		else if(key == 't') key = 'c';
		//Protect the critical section
		pthread_mutex_lock(&mutex);
			data = key; //set data to key
			data_ready = 1; //set data ready flag
		pthread_mutex_unlock(&mutex);
	}
}


int main(int argc, char *argv[]) {
	printf("Fixed Sequence Traffic Lights State Machine Example\n");
	pthread_t th1;
	pthread_create(&th1, NULL, producer_ex, NULL);


	enum states CurrentState = State0;
	int Runtimes=30;
	counter = 0;

	// State machine setup
	while (counter < Runtimes)
	{
		TrafficLight_SM(&CurrentState); // pass address
		counter++;
	}
	//pthread_join (th1, NULL);
	return EXIT_SUCCESS;
}

void TrafficLight_SM(enum states* CurrentState){
	enum states CurrState = *CurrentState;
	printf("Console current state: State%d\n", CurrState);
	char *lightValue[10] = {"Rd", "Yl", "Gr", "FR", "Of"};
	char outputStr[100] = "";
	char input = '0';
	int j;
	int StateType = 1;

	for(j = 0; j < 8; j++){
		strcpy(outputStr,lights[j]);
		strcat(outputStr,"%s\t");

		if (StateType == 1){
			printf(outputStr, lightValue[off_pk_StateCodes[CurrState][j]]);
//			Update central controller
		}
		else{
			printf(outputStr, lightValue[peak_StateCodes[CurrState][j]]);

		}

	}
	printf("\n");

	//Protect the variable data and data_ready
	pthread_mutex_lock(&mutex);
	if (data_ready == 1) {
		printf("Consumer received: %c\n", data);
		input = data;
		data_ready = 0;
	}
	pthread_mutex_unlock(&mutex);

	//Check Input data read works for peak state machine
	//Case of peak hour state machine
	if(StateType == 0){
		//Input data read can now be used to change state
		if(input == 'c'){
			if ((1 <= CurrState && CurrState <= 3) ||  (13 <= CurrState && CurrState <= 15)) CurrState += 4;
			else if(CurrState == 12) CurrState = 20;
			else if(CurrState == 0) CurrState = 8;
		}
		//Condition changes for timeouts that don't care about congestion sensor
		else{
			if((0 <= CurrState && CurrState <= 3) || (8 <= CurrState && CurrState <= 15) ||
					(20 <= CurrState && CurrState <= 22)) CurrState++;
			else if(CurrState == 4) CurrState = 12;
			else if ((5 <= CurrState && CurrState <= 7) || (17 <= CurrState && CurrState <= 19)) CurrState += 4;
			else if (CurrState == 16 || CurrState == 23) CurrState = 0;
		}
	}

	else if(StateType == 1){
		//Input data read can now be used to change state
		if(input == 'c'){
			if (1 <= CurrState && CurrState <= 3) CurrState += 5;
			else if(CurrState == 5) CurrState = 15;
			else if(CurrState == 0) CurrState = 9;
			else if(CurrState == 13) CurrState = 14;
		}
		else if(input == 't'){
			if(CurrState == 15) CurrState = 16;
			else if(CurrState == 13) CurrState = 18;
		}
		//Condition changes for timeouts that don't care about congestion sensor
		else{
			if((0 <= CurrState && CurrState <= 4) || (9 <= CurrState && CurrState <= 12) ||
					(CurrState == 14 || CurrState == 16 || CurrState == 18)) CurrState++;
			else if(CurrState == 5|| CurrState == 15 || CurrState == 13) CurrState = 13;
			else if (6 <= CurrState && CurrState <= 8) CurrState += 4;
			else if (CurrState == 17 || CurrState == 19) CurrState = 0;
		}
	}


	//If one of the lights is green, hold until a key n or e is read
	sleep(1);
	//printf("\n\033[2J\n");
	*CurrentState = CurrState;
}

