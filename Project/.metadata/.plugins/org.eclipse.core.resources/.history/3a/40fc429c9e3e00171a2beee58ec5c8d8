#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>

#include <errno.h>
#include <sys/dispatch.h>
#include <fcntl.h>
#include <share.h>

// Define where the channel is located
// only need to use one of these (depending if you want to use QNET networking or running it locally):
#define LOCAL_ATTACH_POINT "3436413_attach_point"		  			     // change myname to the same name used for the server code.
//#define QNET_ATTACH_POINT  "/net/Beagle03.net.intra/dev/name/local/3436413_attach_point"  // hostname using full path, change myname to the name used for server
#define QNET_ATTACH_POINT  "3436413_attach_point"  // hostname using full path, change myname to the name used for server

#define BUF_SIZE 1000

// Number of phases such as peak, off-peak, night-time etc.
#define NUM_ROUTINES 2

//Number of states per state machine
#define NUM_STATES 24

// Number of variables per state.
#define STATE_LEN 12


typedef struct
{
	struct _pulse hdr; // Our real data comes after this header
	int ClientID; // our data (unique id from client)
    int data;     // our data
} my_data;

typedef struct
{
	struct _pulse hdr; // Our real data comes after this header
    char buf[BUF_SIZE];// Message we send back to clients to tell them the messages was processed correctly.
    int nxt_SM[][NUM_ROUTINES][NUM_STATES][STATE_LEN];
} my_reply;


//Labels of the different states matched to the state machine in report
enum states {
		State0, State1, State2, State3, State4, State5, State6, State7, State8, State9,
		State10, State11, State12, State13, State14, State15, State16, State17, State18, State19,
		State20, State21, State22, State23
}; enum states CurrentState;

//Light definitions in order: North South East West EastSouth WestSouth NSPedestrian EWPedestrian
const char * lights[] = { "N:","S:", "E:","W:","ES:", "WS:", "NSp:", "EWp:"};

//Light states in order from 0 to 4 are red, yellow, green, flashing red and off
enum light_config{
	RED, YELLOW, GREEN, FL_RED, OFF
};

//Corresponding string values of light states
const char *lightValue[10] = {"R ", "Y ", "G ", "FR", "Off"};


//The codes of each state in each state machine, includes the light states of each light, the
//next state after receiving either no condition, a congestion or a traffic sensor trigger.
//The sensor trigger should be either polled or through an interrupt which creates a fast response time
volatile int StateCodes[2][24][12] = {
//Peak Codes
		{{RED, RED, RED, RED, RED, RED, RED, RED,State1,State8,NULL,3},
		{RED, RED, GREEN, GREEN, OFF, OFF, RED, GREEN,State2,State5,NULL,5},
		{RED, RED, GREEN, GREEN, OFF, OFF, RED, FL_RED,State3,State6,NULL,5},
		{RED, RED, GREEN, GREEN, OFF, OFF, RED, RED,State4,State7,NULL,1},
		{RED, RED, YELLOW, YELLOW, YELLOW, YELLOW, RED, RED,State12,NULL,NULL,2},
		{RED, RED, GREEN, GREEN, YELLOW, YELLOW, RED, GREEN,State9,NULL,NULL,2},
		{RED, RED, GREEN, GREEN, YELLOW, YELLOW, RED, FL_RED,State10,NULL,NULL,2},
		{RED, RED, GREEN, GREEN, YELLOW, YELLOW, RED, RED,State11,NULL,NULL,2},
		{RED, RED, GREEN, GREEN, RED, RED, RED, GREEN,State9,NULL,NULL,5},
		{RED, RED, GREEN, GREEN, RED, RED, RED, FL_RED,State10,NULL,NULL,4},
		{RED, RED, GREEN, GREEN, RED, RED, RED, RED,State11,NULL,NULL,1},
		{RED, RED, YELLOW, YELLOW, RED, RED, RED, RED,State12,NULL,NULL,2},
		{RED, RED, RED, RED, RED, RED, RED, RED,State13,State20,NULL,3},
		{GREEN, GREEN, RED, RED, RED, RED, GREEN, RED,State14,State17,NULL,5},
		{GREEN, GREEN, RED, RED, RED, RED, FL_RED, RED,State15,State18,NULL,4},
		{GREEN, GREEN, RED, RED, RED, RED, RED, RED,State16,State19,NULL,1},
		{YELLOW, YELLOW, RED, RED, RED, RED, RED, RED,State0,NULL,NULL,2},
		{GREEN, YELLOW, RED, RED, RED, RED, GREEN, RED,State21,NULL,NULL,2},
		{GREEN, YELLOW, RED, RED, RED, RED, FL_RED, RED,State22,NULL,NULL,2},
		{GREEN, YELLOW, RED, RED, RED, RED, RED, RED,State23,NULL,NULL,2},
		{GREEN, RED, RED, RED, RED, RED, GREEN, RED,State21,NULL,NULL,5},
		{GREEN, RED, RED, RED, RED, RED, FL_RED, RED,State22,NULL,NULL,4},
		{GREEN, RED, RED, RED, RED, RED, RED, RED,State23,NULL,NULL,1},
		{YELLOW, RED, RED, RED, RED, RED, RED, RED,State0,NULL,NULL,2}},

//Off Peak codes
		{{RED, RED, RED, RED, RED, RED, RED, RED,State1,State8,NULL,3},
		{RED, RED, GREEN, GREEN, OFF, OFF, RED, GREEN,State2,State5,NULL,5},
		{RED, RED, GREEN, GREEN, OFF, OFF, RED, FL_RED,State3,State6,NULL,5},
		{RED, RED, GREEN, GREEN, OFF, OFF, RED, RED,State4,State7,NULL,1},
		{RED, RED, YELLOW, YELLOW, YELLOW, YELLOW, RED, RED,State12,NULL,NULL,2},
		{RED, RED, GREEN, GREEN, YELLOW, YELLOW, RED, GREEN,State9,NULL,NULL,2},

		{RED, RED, GREEN, GREEN, YELLOW, YELLOW, RED, FL_RED,State10,NULL,NULL,2},
		{RED, RED, GREEN, GREEN, YELLOW, YELLOW, RED, RED,State11,NULL,NULL,2},
		{RED, RED, GREEN, GREEN, RED, RED, RED, GREEN,State9,NULL,NULL,5},
		{RED, RED, GREEN, GREEN, RED, RED, RED, FL_RED,State10,NULL,NULL,4},
		{RED, RED, GREEN, GREEN, RED, RED, RED, RED,State11,NULL,NULL,1},

		{RED, RED, YELLOW, YELLOW, RED, RED, RED, RED,State12,NULL,NULL,2},
		{RED, RED, RED, RED, RED, RED, RED, RED,State13,State15,NULL,3},
		{GREEN, GREEN, RED, RED, RED, RED, GREEN, RED,State13,State14,State16,5},
		{GREEN, YELLOW, RED, RED, RED, RED, GREEN, RED,State15,NULL,NULL,2},
		{GREEN, RED, RED, RED, RED, RED, GREEN, RED,State13,State15,State17,5},

		{GREEN, GREEN, RED, RED, RED, RED, FL_RED, RED,State19,NULL,NULL,4},
		{GREEN, RED, RED, RED, RED, RED, FL_RED, RED,State18,NULL,NULL,4},
		{GREEN, GREEN, RED, RED, RED, RED, FL_RED, RED,State0,NULL,NULL,1},
		{YELLOW, YELLOW, RED, RED, RED, RED, RED, RED,State0,NULL,NULL,1}}
};

//Global variables
volatile int StateType = 1; // Sets the state machine number being run (not changed as yet)
volatile char data = 0; //Data
volatile int data_ready = 0; // Flag

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void *producer_ex(void *notused) {
	printf("Producer Started....\n");
	char key = 'c';
	while (1) {
		sleep(2);
		if(key == 'c') key = 'n';
		else if(key == 'n') key = 't';
		else if(key == 't') key = 'c';
		//Protect the critical section
		pthread_mutex_lock(&mutex);
			data = key; //set data to key
			data_ready = 1; //set data ready flag
		pthread_mutex_unlock(&mutex);
	}
	return NULL;
}


int main(int argc, char *argv[]) {
	printf("Intersection 1 State Machine\n"); //Print title to console

	//Initiate and create thread for receiving input sensor values
	pthread_t th1;
	pthread_create(&th1, NULL, producer_ex, NULL);

	//Initialize variables used in state machine
	int i;
	char outputStr[100] = "";
	char input = 'n';
	CurrentState = State0;
	int *b = 2;
	int new_SM[NUM_ROUTINES][NUM_STATES][STATE_LEN];

	my_data msg;
	my_reply reply;

	msg.ClientID = 600; // unique number for this client (optional)

	int server_coid;
	int index = 0;

	char * sname = QNET_ATTACH_POINT;

	printf("  ---> Trying to connect to server named: %s\n", sname);
	if ((server_coid = name_open(sname, 0)) == -1)
	{
		printf("\n    ERROR, could not connect to server!\n\n");
		return EXIT_FAILURE;
	}

    printf("Connection established to: %s\n", sname);

    // We would have pre-defined data to stuff here
    msg.hdr.type = 0x00;
    msg.hdr.subtype = 0x00;


	// State machine setup
	while (1)
	{
		printf("Console current state: State%d\n", CurrentState); // Print current state to console

		//Print output string to console, includes the various light states in the current state
		for (i = 0; i < 8; i++) {
			strcpy(outputStr, lights[i]);
			strcat(outputStr, "%s\t");
			printf(outputStr, lightValue[StateCodes[StateType][CurrentState][i]]);
		}
		printf("\n");

		//State Machine state transition code
		sleep(StateCodes[StateType][CurrentState][11]); //sleep for length specified in state code

		//Protect the variable data and data_ready and check if data has been changed
		pthread_mutex_lock(&mutex);
		if (data_ready == 1) {
			printf("Consumer received: %c\n", data);
			input = data;
			data_ready = 0;
		}
		pthread_mutex_unlock(&mutex);

		msg.data = CurrentState;
		if (MsgSend(server_coid, &msg, sizeof(msg), &reply, sizeof(reply)) == -1)
		{
			printf(" Error data '%d' NOT sent to server\n", msg.data);
		}
        else
        { // now process the reply
            printf("   -->Reply is: '%s'\n", reply.buf);
        	if (strcmp(reply.buf, "Resume") != 0){
        		new_SM = reply.nxt_SM;
//        		StateCodes = new_SM[2][];
        		printf("new_SM: %d", &new_SM);
        	}
        }

		//Input data read can now be used to change state
		if (input == 'c' && StateCodes[StateType][CurrentState][9] != NULL)
			CurrentState = StateCodes[StateType][CurrentState][9];
		else if (input == 't' && StateCodes[StateType][CurrentState][10] != NULL)
			CurrentState = StateCodes[StateType][CurrentState][10];
		else CurrentState = StateCodes[StateType][CurrentState][8];

//		printf("Client (ID:%d), sending data packet with the integer value: %d \n", msg.ClientID, msg.data);
//		fflush(stdout);

//		sleep(1);

	}
	//pthread_join (th1, NULL);
//	return EXIT_SUCCESS;
}
