/*
 * LCDinc.h
 *
 *  Created on: 01/06/2017
 *      Author: s9707730
 */

#ifndef LCDINC_H_
#define LCDINC_H_

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <fcntl.h>
#include <devctl.h>
#include <hw/i2c.h>
#include <errno.h>
#include <unistd.h>
#include <sys/neutrino.h>

#define DATA_SEND 0x40 // sets the Rs value high
#define Co_Ctrl 0x00 // mode to tell LCD we are sending a single command


int initI2C(uint8_t I2Cadd);
void Initialise_LCD (int fd, _Uint32t LCDi2cAdd);
int I2cWrite_(int fd, uint8_t Address, uint8_t mode, uint8_t *pBuffer, uint32_t NbData);
void SetCursor(int fd, uint8_t LCDi2cAdd, uint8_t row, uint8_t column);
void LCDLine1(int fd, uint8_t LCDi2cAdd);
void LCDLine2(int fd, uint8_t LCDi2cAdd);

#endif /* LCDINC_H_ */
