/*
 * LCDinc.c
 *
 *  Created on: 01/06/2017
 *      Author: s9707730
 */


#include "LCDinc.h"

int initI2C(uint8_t I2Cadd)
{
	_Uint32t speed = 10000; // nice and slow (will work with 200000)
	int file;
	int error;

	// Open I2C resource and set it up
	if ((file = open("/dev/i2c1",O_RDWR)) < 0) // OPEN I2C1
		printf("Error while opening Device File.!!\n");
	else
		printf("I2C1 Opened Successfully\n");

	error = devctl(file,DCMD_I2C_SET_BUS_SPEED,&(speed),sizeof(speed),NULL); // Set Bus speed
	if(error)
	{
		fprintf(stderr, "Error setting the bus speed: %d\n",strerror ( error ));
		exit(EXIT_FAILURE);
	}
	else
		printf("Bus speed set = %d\n", speed);

	Initialise_LCD(file, I2Cadd); // initialise the LCD module

	return file;
}

void Initialise_LCD (int fd, _Uint32t LCDi2cAdd)
{
	uint8_t LCDcontrol = 0x00;
	// Initialise the LCD display via the I2C bus
	LCDcontrol = 0x38; // data byte for FUNC_SET_TBL1
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1); // write 0x38 to control reg
	LCDcontrol = 0x39; // data byte for FUNC_SET_TBL2
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1); // write 0x39 to control reg
	LCDcontrol = 0x14; // data byte for Internal OSC frequency
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1); // write 0x14 to control reg
	LCDcontrol = 0x79; // data byte for contrast setting
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1); // write 0x79 to control reg
	LCDcontrol = 0x50; // data byte for Power/ICON control Contrast set
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1); // write 0x50 to control reg
	LCDcontrol = 0x6C; // data byte for Follower control
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1); // write 0x6C to control reg
	LCDcontrol = 0x0C; // data byte for Display ON
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1); // write 0x0C to control reg
	LCDcontrol = 0x01; // data byte for Clear display
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1); // write 0x01 to control reg
}

// Writes to I2C
int I2cWrite_(int fd, uint8_t Address, uint8_t mode, uint8_t *pBuffer, uint32_t NbData)
{
	i2c_send_t hdr;
	iov_t sv[2];
	int status, i;
	uint8_t LCDpacket[21] = {}; // limited to 21 characters (1 control bit + 20 bytes)
	// set the mode for the write (control or data)
	LCDpacket[0] = mode; // set the mode (data or control)
	// copy data to send to send buffer (after the mode bit)
	for(i=0;i<NbData+1;i++)
		LCDpacket[i+1] = *pBuffer++;
	hdr.slave.addr = Address;
	hdr.slave.fmt = I2C_ADDRFMT_7BIT;
	hdr.len = NbData + 1; // 1 extra for control (mode) bit
	hdr.stop = 1;
	SETIOV(&sv[0], &hdr, sizeof(hdr));
	SETIOV(&sv[1], &LCDpacket[0], NbData + 1); // 1 extra for control (mode) bit
	status = devctlv(fd, DCMD_I2C_SEND, 2, 0, sv, NULL, NULL);
	if (status != EOK)
		printf("status = %s\n", strerror ( status ));
	return status;
}

void SetCursor(int fd, uint8_t LCDi2cAdd, uint8_t row, uint8_t column)
{
	uint8_t position = 0x80; // SET_DDRAM_CMD (control bit)
	uint8_t rowValue = 0;
	uint8_t LCDcontrol = 0;
	if (row == 1)
		rowValue = 0x40; // memory location offset for row 1
	position = (uint8_t)(position + rowValue + column);
	LCDcontrol = position;
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1); // write data to I2C
}

void LCDLine1(int fd, uint8_t LCDi2cAdd)
{
	uint8_t LCDcontrol = 0x80;

	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1); // write data to I2C
}


void LCDLine2(int fd, uint8_t LCDi2cAdd)
{
	uint8_t LCDcontrol = 0xC0;

	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1); // write data to I2C
}
