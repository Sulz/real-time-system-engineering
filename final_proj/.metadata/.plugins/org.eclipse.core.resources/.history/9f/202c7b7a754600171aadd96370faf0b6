#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/iofunc.h>
#include <sys/dispatch.h>
#include <share.h>
#include <sys/netmgr.h>
#include "SensorThread.h"
#include "LCDinc.h"


#define ATTACH_POINT "s3238287_project_intersection1"

#define CC_ATTACH_POINT "3436413_attach_point"

#define BUF_SIZE 1000

// Number of phases such as peak, off-peak, night-time etc.
#define NUM_ROUTINES 2

//Number of states per state machine
#define NUM_STATES 24

// Number of variables per state.
#define STATE_LEN 13


typedef struct
{
	struct _pulse hdr; // Our real data comes after this header
	int clientId; // client id number to address specific client
	char clientMessage[100]; // coded message for server to recognize client
} client_code;


typedef struct {
	struct _pulse hdr;  // Our real data comes after this header
	char buf[BUF_SIZE]; // Message we send back to clients to tell them the messages was processed correctly.
} reply;


typedef struct
{
	struct _pulse hdr; // Our real data comes after this header
	int ClientID; // our data (unique id from client)
	int data[13];     // our data
} my_data;

typedef struct
{
	struct _pulse hdr; // Our real data comes after this header
	char buf[BUF_SIZE];// Message we send back to clients to tell them the messages was processed correctly.
	int nxt_SM[NUM_ROUTINES][NUM_STATES][STATE_LEN];
} my_reply;


//Labels of the different states matched to the state machine in report
enum states {
	State0, State1, State2, State3, State4, State5, State6, State7, State8, State9,
	State10, State11, State12, State13, State14, State15, State16, State17, State18, State19,
	State20, State21, State22, State23
};

//Light definitions in order: North South East West EastSouth WestSouth NSPedestrian EWPedestrian
const char * lights[] = { "N:","S:", "E:","W:","ES:", "WS:", "NSp:", "EWp:"};

//Light states in order from 0 to 4 are red, yellow, green, flashing red and off
enum light_config{
	RED, YELLOW, GREEN, FL_RED, OFF
};

//Corresponding string values of light states
const char *lightValue[10] = {"R,", "Y,", "G,", "FR,", "Of,"};


//The codes of each state in each state machine, includes the light states of each light, the
//next state after receiving either no condition, a congestion or a traffic sensor trigger.
//The sensor trigger should be either polled or through an interrupt which creates a fast response time
int StateCodes[2][24][13] = {
		//Peak Codes
		{{RED, RED, RED, RED, RED, RED, RED, RED,State1,State8,NULL,3,NULL},
				{RED, RED, GREEN, GREEN, OFF, OFF, RED, GREEN,State2,State5,NULL,5,NULL},
				{RED, RED, GREEN, GREEN, OFF, OFF, RED, FL_RED,State3,State6,NULL,5,NULL},
				{RED, RED, GREEN, GREEN, OFF, OFF, RED, RED,State4,State7,NULL,1,NULL},
				{RED, RED, YELLOW, YELLOW, YELLOW, YELLOW, RED, RED,State12,NULL,NULL,2,NULL},
				{RED, RED, GREEN, GREEN, YELLOW, YELLOW, RED, GREEN,State9,NULL,NULL,2,NULL},
				{RED, RED, GREEN, GREEN, YELLOW, YELLOW, RED, FL_RED,State10,NULL,NULL,2,NULL},
				{RED, RED, GREEN, GREEN, YELLOW, YELLOW, RED, RED,State11,NULL,NULL,2,NULL},
				{RED, RED, GREEN, GREEN, RED, RED, RED, GREEN,State9,NULL,NULL,5,NULL},
				{RED, RED, GREEN, GREEN, RED, RED, RED, FL_RED,State10,NULL,NULL,4,NULL},
				{RED, RED, GREEN, GREEN, RED, RED, RED, RED,State11,NULL,NULL,1,NULL},
				{RED, RED, YELLOW, YELLOW, RED, RED, RED, RED,State12,NULL,NULL,2,NULL},
				{RED, RED, RED, RED, RED, RED, RED, RED,State13,State20,NULL,3,NULL},
				{GREEN, GREEN, RED, RED, RED, RED, GREEN, RED,State14,State17,NULL,5,NULL},
				{GREEN, GREEN, RED, RED, RED, RED, FL_RED, RED,State15,State18,NULL,4,NULL},
				{GREEN, GREEN, RED, RED, RED, RED, RED, RED,State16,State19,NULL,1,NULL},
				{YELLOW, YELLOW, RED, RED, RED, RED, RED, RED,State0,NULL,NULL,2,NULL},
				{GREEN, YELLOW, RED, RED, RED, RED, GREEN, RED,State21,NULL,NULL,2,NULL},
				{GREEN, YELLOW, RED, RED, RED, RED, FL_RED, RED,State22,NULL,NULL,2,NULL},
				{GREEN, YELLOW, RED, RED, RED, RED, RED, RED,State23,NULL,NULL,2,NULL},
				{GREEN, RED, RED, RED, RED, RED, GREEN, RED,State21,NULL,NULL,5,NULL},
				{GREEN, RED, RED, RED, RED, RED, FL_RED, RED,State22,NULL,NULL,4,NULL},
				{GREEN, RED, RED, RED, RED, RED, RED, RED,State23,NULL,NULL,1,NULL},
				{YELLOW, RED, RED, RED, RED, RED, RED, RED,State0,NULL,NULL,2,NULL}},

				//Off Peak codes
				{{RED, RED, RED, RED, RED, RED, RED, RED,State1,State8,NULL,3,NULL},
						{RED, RED, GREEN, GREEN, OFF, OFF, RED, GREEN,State2,State5,NULL,5,NULL},
						{RED, RED, GREEN, GREEN, OFF, OFF, RED, FL_RED,State3,State6,NULL,5,NULL},
						{RED, RED, GREEN, GREEN, OFF, OFF, RED, RED,State4,State7,NULL,1,NULL},
						{RED, RED, YELLOW, YELLOW, YELLOW, YELLOW, RED, RED,State12,NULL,NULL,2,NULL},
						{RED, RED, GREEN, GREEN, YELLOW, YELLOW, RED, GREEN,State9,NULL,NULL,2,NULL},

						{RED, RED, GREEN, GREEN, YELLOW, YELLOW, RED, FL_RED,State10,NULL,NULL,2,NULL},
						{RED, RED, GREEN, GREEN, YELLOW, YELLOW, RED, RED,State11,NULL,NULL,2,NULL},
						{RED, RED, GREEN, GREEN, RED, RED, RED, GREEN,State9,NULL,NULL,5,NULL},
						{RED, RED, GREEN, GREEN, RED, RED, RED, FL_RED,State10,NULL,NULL,4,NULL},
						{RED, RED, GREEN, GREEN, RED, RED, RED, RED,State11,NULL,NULL,1,NULL},

						{RED, RED, YELLOW, YELLOW, RED, RED, RED, RED,State12,NULL,NULL,2,NULL},
						{RED, RED, RED, RED, RED, RED, RED, RED,State13,State15,NULL,3,NULL},
						{GREEN, GREEN, RED, RED, RED, RED, GREEN, RED,State13,State14,State16,5,NULL},
						{GREEN, YELLOW, RED, RED, RED, RED, GREEN, RED,State15,NULL,NULL,2,NULL},
						{GREEN, RED, RED, RED, RED, RED, GREEN, RED,State13,State15,State17,5,NULL},

						{GREEN, GREEN, RED, RED, RED, RED, FL_RED, RED,State19,NULL,NULL,4,NULL},
						{GREEN, RED, RED, RED, RED, RED, FL_RED, RED,State18,NULL,NULL,4,NULL},
						{GREEN, GREEN, RED, RED, RED, RED, FL_RED, RED,State0,NULL,NULL,1,NULL},
						{YELLOW, YELLOW, RED, RED, RED, RED, RED, RED,State0,NULL,NULL,1,NULL}}
};



//Global variables
volatile int StateType = 0; // Sets the state machine number being run
volatile char data = 0; //Data
volatile int data_ready = 0; // Flag
volatile bool TrafficSens = 0;		// Traffic sensor shared memory
volatile bool ConjestionSens = 0;	// Congestion sensor Shared memory

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
name_attach_t *attach;
reply replymsg;
client_code msg;
int rcvid = 0;

/*////////////////////////////////////////////////
 * Initialization of server function
 *////////////////////////////////////////////////
int initiateServer(){
	// Create a named channel for this node to receive timer pulses
	if ((attach = name_attach(NULL, ATTACH_POINT, 0)) == NULL) {
		printf("\nFailed to name_attach on ATTACH_POINT: %s \n", ATTACH_POINT);
		return EXIT_FAILURE;
	}
	printf("Server Listening for Clients on ATTACH_POINT: %s \n", ATTACH_POINT);

	replymsg.hdr.type = 0x01;
	replymsg.hdr.subtype = 0x00;

	return EXIT_SUCCESS;
}


int main(int argc, char *argv[]) {
	printf("Intersection 1 State Machine\n"); //Print title to console

	int file;
	volatile uint8_t LCDi2cAdd = 0x3c;

	uint8_t LCDdata[21] = {};

	file = initI2C(LCDi2cAdd);

	my_data CC_msg;
	my_reply CC_reply;

	pthread_t th1;
	// Spawn sensorThread
	pthread_create (&th1, NULL, SensorThread, NULL);

	//Initialize variables used in state machine
	int i, j, k;
	char outputStr[100] = "";
	enum states CurrentState = State0;

	char lineStr1[100];
	char lineStr2[100];


	int server_coid;
	CC_msg.ClientID = 600;

	initiateServer();

	char * sname = CC_ATTACH_POINT;

	printf("  ---> Trying to connect to server named: %s\n", sname);
	if ((server_coid = name_open(sname, 0)) == -1)
	{
		printf("\n    ERROR, could not connect to server!\n\n");
		return EXIT_FAILURE;
	}

	printf("Connection established to: %s\n", sname);

	// State machine setup
	while (1)
	{
		printf("Console current state: State%d\n", CurrentState); // Print current state to console

//		printf("Should see this\n\n");
//		fflush(stdout);
		lineStr1[0] = '\0';
		lineStr2[0] = '\0';

		//Print output string to console, includes the various light states in the current state
		for (i = 0; i < 5; i++) {
			strcpy(outputStr, lights[i]);
			strcat(outputStr,lightValue[StateCodes[StateType][CurrentState][i]]);
			strcat(lineStr1, outputStr);
		}
		printf(lineStr1);
		printf("\n");
		fflush(stdout);

		for (i = 5; i < 8; i++) {
			strcpy(outputStr, lights[i]);
			strcat(outputStr,lightValue[StateCodes[StateType][CurrentState][i]]);
			strcat(lineStr2, outputStr);
		}

		printf(lineStr2);
		printf("\n");
		fflush(stdout);

		printf("\n\n");
		time_t  now;
		time(&now);
		printf("Current time: %s\n", ctime(&now));


		LCDLine1(file, LCDi2cAdd);
		strcpy(LCDdata, lineStr1);
		I2cWrite_(file, LCDi2cAdd, DATA_SEND, &LCDdata[0], sizeof(LCDdata));// write new data to I2C

		LCDLine2(file, LCDi2cAdd);
		strcpy(LCDdata,lineStr2);
		I2cWrite_(file, LCDi2cAdd, DATA_SEND, &LCDdata[0], sizeof(LCDdata));// write new data to I2C

		//Wait for a timer pulse from the timer node (it happens twice for some reason)
		//Length of wait specified in state code
		for(i=0; i < 2*StateCodes[StateType][CurrentState][11]; i++){
			rcvid = MsgReceive(attach->chid, &msg, sizeof(msg), NULL);
			if (rcvid > 0){
				if (!strcmp(msg.clientMessage, "Time")) {
					StateType = 1;
					CurrentState = State0;
					printf("Are we here1\n");
				}
				MsgReply(rcvid, EOK, &replymsg, sizeof(replymsg));
			}
		}

		for (i = 0; i < 13; i++){
			CC_msg.data[i] = StateCodes[StateType][CurrentState][i];
		}

		if (MsgSend(server_coid, &CC_msg, sizeof(CC_msg), &CC_reply, sizeof(CC_reply)) == -1)
		{
			printf(" Error data '%d' NOT sent to server\n", CC_msg.data);
		}
		else
		{ // now process the reply
			printf("   -->Reply is: '%s'\n", CC_reply.buf);
			if (strcmp(CC_reply.buf, "Resume") != 0){
				printf("Are we here??\n\n\n\n");
				for (i = 0; i < 2; i++){
					for (k = 0; k < 24; k++){
						for (j = 0; j < 13; j++){
							StateCodes[i][k][j] = CC_reply.nxt_SM[i][k][j];
						}
					}
				}
			}
		}

		//sleep(1);

		//Change state depending on state codes
		if (StateCodes[StateType][CurrentState][9] != NULL || StateCodes[StateType][CurrentState][10] != NULL) {
			//Protect the variable data and data_ready and check if data has been changed
			pthread_mutex_lock(&mutex);
			if (ConjestionSens && StateCodes[StateType][CurrentState][9] != NULL)
				CurrentState = StateCodes[StateType][CurrentState][9];
			else if (TrafficSens && StateCodes[StateType][CurrentState][10] != NULL)
				CurrentState = StateCodes[StateType][CurrentState][10];
			else
				CurrentState = StateCodes[StateType][CurrentState][8];
			pthread_mutex_unlock(&mutex);
		} else
			CurrentState = StateCodes[StateType][CurrentState][8];


		printf("\n\033[2J\n"); //Generates a clear in the output console
	}
	name_detach(attach, 0);
	return EXIT_SUCCESS;
}

