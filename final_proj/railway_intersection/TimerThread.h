/*
 * TimerThread.h
 *
 *  Created on: May 21, 2017
 *      Author: dave
 */

#ifndef TIMERTHREAD_H_
#define TIMERTHREAD_H_

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <sys/netmgr.h>
#include <sys/neutrino.h>
#include "stdbool.h"
#include <errno.h>
#include <semaphore.h>

//char *progname = "timer_per1.c";

#define MY_PULSE_CODE   _PULSE_CODE_MINAVAIL

typedef union
{
	struct _pulse   pulse;
    // your other message structures would go here too
} my_message_t;

typedef struct {
int sec;
int nsec;
} Tim_Val;

sem_t State0Sem;

pthread_mutex_t StatemachineProtect;

void *TimerThread (void *data);



#endif /* TIMERTHREAD_H_ */
