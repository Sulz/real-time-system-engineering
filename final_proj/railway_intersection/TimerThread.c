/*
 * TimerThread.c
 *
 *  Created on: May 21, 2017
 *      Author: dave
 */

#include "TimerThread.h"



void *TimerThread (void *data)
{

	Tim_Val *time = (Tim_Val*) data;

	struct sigevent         event;
	struct itimerspec       itime;
	timer_t                 timer_id;
	int                     chid;
	int                     rcvid;
	my_message_t            msg;

	chid = ChannelCreate(0); // Create a communications channel

	event.sigev_notify = SIGEV_PULSE;

	// create a connection back to ourselves for the timer to send the pulse on
	event.sigev_coid = ConnectAttach(ND_LOCAL_NODE, 0, chid, _NTO_SIDE_CHANNEL, 0);
	if (event.sigev_coid == -1)
	{
	   //printf(stderr, "%s:  couldn't ConnectAttach to self!\n", progname);
	   perror(NULL);
	   exit(EXIT_FAILURE);
	}
	event.sigev_priority = getprio(0);
	event.sigev_code = MY_PULSE_CODE;

	// create the timer, binding it to the event
	if (timer_create(CLOCK_REALTIME, &event, &timer_id) == -1)
	{
	   //printf (stderr, "%s:  couldn't create a timer, errno %d\n", progname, errno);
	   perror (NULL);
	   exit (EXIT_FAILURE);
	}

	// setup the timer (1.5s initial delay value, 1.5s reload interval)
	itime.it_value.tv_sec = time->sec;			  // 1 second
	itime.it_value.tv_nsec = time->nsec;    // 500 million nsecs = .5 secs


//	printf("timesec = %d\n", time->sec);

	// and start the timer!
	timer_settime(timer_id, 0, &itime, NULL);

	/*
	* As of the timer_settime(), we will receive our pulse
	* in 1.5 seconds (the itime.it_value) and every 1.5
	* seconds thereafter (the itime.it_interval)
	*/




		// wait for message/pulse
	   rcvid = MsgReceive(chid, &msg, sizeof(msg), NULL);

	   // determine who the message came from
	   if (rcvid == 0) // this process
	   {
		   // received a pulse, now check "code" field...
		   if (msg.pulse.code == MY_PULSE_CODE) // we got a pulse
			{
				// Here we implement a simple state machine
				printf("Timer has timedout\n");
				sem_post(&State0Sem);



				return 0;

			}
	   }

	}
