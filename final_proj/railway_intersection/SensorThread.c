// includes


#include <stdlib.h>
#include <stdio.h>
#include <hw/inout.h> // for in32() and out32();
#include <sys/mman.h> // for mmap_device_io();
#include <stdint.h> // for unit32 types
#include <sys/neutrino.h> // for ThreadCtl( _NTO_TCTL_IO_PRIV , NULL)
#include "SensorThread.h"

// defines

#define AM335X_CONTROL_MODULE_BASE (uint64_t) 0x44E10000
#define AM335X_CONTROL_MODULE_SIZE (size_t) 0x00001448
#define AM335X_GPIO_SIZE (uint64_t) 0x00001000
#define AM335X_GPIO1_BASE (size_t) 0x4804C000
#define LED0 (1<<21) // GPIO1_21
#define LED1 (1<<22) // GPIO1_22
#define LED2 (1<<23) // GPIO1_23
#define LED3 (1<<24) // GPIO1_24
#define GPIO_OE 0x134
#define GPIO_DATAIN 0x138
#define GPIO_DATAOUT 0x13C
#define P9_12_pinConfig 	0x878 // conf_gpmc_ben1 (TRM pp 1364) for GPIO1_28, P9_12 (GPIO1_12)
#define	conf_gpmc_ad12		0x830 // this is GPIO1_12

#define SD0 (1<<12) // SD0 is connected to GPIO1_28
#define SCL (1<<13) // SCL is connected to GPIO1_16

// GPMC_A1_Configuration
#define PIN_MODE_0 0x00
#define PIN_MODE_1 0x01
#define PIN_MODE_2 0x02
#define PIN_MODE_3 0x03
#define PIN_MODE_4 0x04
#define PIN_MODE_5 0x05
#define PIN_MODE_6 0x06
#define PIN_MODE_7 0x07

// PIN MUX Configuration strut values (page 1420 from TRM)
#define PU_ENABLE 0x00
#define PU_DISABLE 0x01
#define PU_PULL_UP 0x01
#define PU_PULL_DOWN 0x00
#define RECV_ENABLE 0x01
#define RECV_DISABLE 0x00
#define SLEW_FAST 0x00
#define SLEW_SLOW 0x01

#define GPIO_OE 0x134
#define GPIO_DATAOUT 0x13C
#define GPIO_IRQSTATUS_SET_0 0x34 // enable interrupt generation
#define GPIO_IRQSTATUS_SET_1 0x38 // enable interrupt generation
#define GPIO_IRQWAKEN_0 0x44 // Wakeup Enable for Interrupt Line
#define GPIO_IRQWAKEN_1 0x48 // Wakeup Enable for Interrupt Line
#define GPIO_FALLINGDETECT 0x14C // set falling edge trigger
#define GPIO_CLEARDATAOUT 0x190 // clear data out Register
#define GPIO_IRQSTATUS_0 0x2C // clear any prior IRQs
#define GPIO_IRQSTATUS_1 0x30 // clear any prior IRQs

#define GPIO1_IRQ 99 // TRG page 465 list the IRQs for the am335x

typedef union _CONF_MODULE_PIN_STRUCT // See TRM Page 1420
{
	unsigned int d32;
	struct { // name: field size
			unsigned int conf_mmode : 3; // LSB
			unsigned int conf_puden : 1;
			unsigned int conf_putypesel : 1;
			unsigned int conf_rxactive : 1;
			unsigned int conf_slewctrl : 1;
			unsigned int conf_res_1 : 13; // reserved
			unsigned int conf_res_2 : 12; // reserved MSB
			} b;

} _CONF_MODULE_PIN;

uint32_t PrevWord = 0;




/* This is the code for the thread.  It is not called directly
 * but passed as a parameter to pthread_create in the main thread.
 * The single void parameter may be used to pass parameters.
*/
 void *SensorThread (void *data)
 {

	 printf("you are here");


	uintptr_t control_module = mmap_device_io(AM335X_CONTROL_MODULE_SIZE, AM335X_CONTROL_MODULE_BASE);
	uintptr_t gpio1_base = mmap_device_io(AM335X_GPIO_SIZE , AM335X_GPIO1_BASE);

	if( (control_module)&&(gpio1_base) )
	 {
	 ThreadCtl( _NTO_TCTL_IO_PRIV , NULL);// Request I/O privileges;

	 uint32_t val = 0;

// set DDR for LEDs to output and GPIO_28 to input
	 val = in32(gpio1_base + GPIO_OE); // read in current setup for GPIO1 port
	 val |= 1<<28; // set IO_BIT_28 high (1=input, 0=output)
	 out32(gpio1_base + GPIO_OE, val); // write value to input enable for data pins
	 val &= ~(LED0|LED1|LED2|LED3); // write value to output enable
	 out32(gpio1_base + GPIO_OE, val); // write value to output enable for LED pins

	 in32s(&val, 1, control_module + conf_gpmc_ad12 );
	 printf("Original pinmux configuration for GPIO1_28 = %#010x\n", val);


// set up pin mux for the pins we are going to use (see page 1354 of TRM)
	 volatile _CONF_MODULE_PIN pinConfigGPMC; // Pin configuration strut
	 pinConfigGPMC.d32 = 0;

// Pin MUX register default setup for input (GPIO input, disable pull up/down - Mode 7)
	 pinConfigGPMC.b.conf_slewctrl = SLEW_SLOW; // Select between faster or slower slew rate
	 pinConfigGPMC.b.conf_rxactive = RECV_ENABLE; // Input enable value for the PAD
	 pinConfigGPMC.b.conf_putypesel= PU_PULL_UP; // Pad pullup/pulldown type selection
	 pinConfigGPMC.b.conf_puden = PU_ENABLE; // Pad pullup/pulldown enable
	 pinConfigGPMC.b.conf_mmode = PIN_MODE_7; // Pad functional signal mux select 0 - 7

// Write to PinMux registers for the GPIO1_28
	 out32(control_module + conf_gpmc_ad12, pinConfigGPMC.d32);
	 in32s(&val, 1, control_module + conf_gpmc_ad12); // Read it back
	 printf("New configuration register for GPIO1_28 = %#010x\n", val);

	 val = in32(gpio1_base + GPIO_OE);
	 val &= ~SCL; // 0 for output
	 out32(gpio1_base + GPIO_OE, val); // write value to output enable for data pins

	 val = in32(gpio1_base + GPIO_DATAOUT);
	 val |= SCL; // Set Clock Line High as per TTP229-BSF datasheet
	 out32(gpio1_base + GPIO_DATAOUT, val); // for 16-Key active-Low timing diagram

	 // Setup IRQ for SD0 pin ( see TRM page 4871 for register list)
	  out32(gpio1_base + GPIO_IRQSTATUS_SET_1, SD0);// Write 1 to GPIO_IRQSTATUS_SET_1
	  out32(gpio1_base + GPIO_IRQWAKEN_1, SD0); // Write 1 to GPIO_IRQWAKEN_1
	  out32(gpio1_base + GPIO_FALLINGDETECT, SD0); // set falling edge
	  out32(gpio1_base + GPIO_CLEARDATAOUT, SD0); // clear GPIO_CLEARDATAOUT
	  out32(gpio1_base + GPIO_IRQSTATUS_1, SD0); // clear any prior IRQs

	  struct sigevent event; // fill in "event" structure
	  memset(&event, 0, sizeof(event));
	  event.sigev_notify = SIGEV_INTR; // Setup for external interrupt
	  int id = 0; // Attach interrupt Event to IRQ for GPIO1B (upper 16 bits of port)
	  id = InterruptAttachEvent (GPIO1_IRQ, &event, _NTO_INTR_FLAGS_TRK_MSK);

	  // Main code starts here
	  printf( "Waiting For Interrupt 99 - key press on Jaycar (XC4602) keypad\n");

	 int i = 0;

	 for(;;) // for loop that correctly decodes key press
	 {
		 volatile uint32_t word = 0;

		 InterruptWait( 0, NULL ); // block this thread until an interrupt occurs
		 InterruptDisable();

		 // confirm that SD0 is still low (that is a valid Key press event has occurred)
		 val = KeypadReadIObit(gpio1_base, SD0); // read SD0 (means data is ready)
		 if(val == 0) // start reading key value form the keypad
		 {
			 word = 0; // clear word variable
			 delaySCL(); // wait a short period of time before reading the data Tw (10 us)

			 for(i=0;i<16;i++) // get data from SD0 (16 bits)
			 {
				 strobe_SCL(gpio1_base); // strobe the SCL line so we can read in data bit

				 val = KeypadReadIObit(gpio1_base, SD0); // read in data bit
				 val = ~val & 0x01; // invert bit and mask out everything but the LSB

				 word = word | (val<<i); // add data bit to word in unique position (build word up bit by bit)
			 }


			 DecodeKeyValue(word);
		 }


		 out32(gpio1_base + GPIO_IRQSTATUS_0, SD0); //clear IRQ
		  InterruptUnmask(GPIO1_IRQ, id);
		  InterruptEnable();
	 }



	  // will never get here
	  munmap_device_io(control_module, AM335X_CONTROL_MODULE_SIZE);
	  }
	return 0;
 }

 //##########################################################################################
 //
 //
 //##########################################################################################
 void strobe_SCL(uintptr_t gpio_port_add)
 {
 	uint32_t PortData;
 	PortData = in32(gpio_port_add + GPIO_DATAOUT);// value that is currently on the GPIO port
 	PortData &= ~(SCL);
 	out32(gpio_port_add + GPIO_DATAOUT, PortData);// Clock low
 	delaySCL();
 	PortData = in32(gpio_port_add + GPIO_DATAOUT);// get port value
 	PortData |= SCL;// Clock high
 	out32(gpio_port_add + GPIO_DATAOUT, PortData);
 	delaySCL();
 }


 //##########################################################################################
 //
 //
 //##########################################################################################
 void delaySCL()

 {// Small delay used to get timing correct for BBB



 	volatile int i, a;
 	for(i=0;i<0x1F;i++) // 0x1F results in a delay that sets F_SCL to ~480 kHz
 	{ // i*1 is faster than i+1 (i+1 results in F_SCL ~454 kHz, whereas i*1 is the same as a=i)
 		a = i;
 	}

 	// usleep(1); //why doesn't this work? Ans: Results in a period of 4ms as
 	// fastest time, which is 250Hz (This is to slow for the TTP229 chip as it
 	// requires F_SCL to be between 1 kHz and 512 kHz)
 }


 //##########################################################################################
 //
 //
 //##########################################################################################
 uint32_t KeypadReadIObit(uintptr_t gpio_base, uint32_t BitsToRead)

 {
 	volatile uint32_t val = 0;
 	val = in32(gpio_base + GPIO_DATAIN);// value that is currently on the GPIO port
 	val &= BitsToRead; // mask bit
 	if(val==BitsToRead)
 		return 1;
 	else return 0;
 }


 //##########################################################################################
 // Decodes keypad serial data
 // Button 1 sets pedestrian shared memory - state machine must clear this
 // Button 12 triggers train sensor, button 12 release will clear
 // Button 16 triggers boom down sensor, button 12 release will clear
 //##########################################################################################
 void DecodeKeyValue(uint32_t word)
 {
 	uint32_t tempWord = word;		// save word value for end of function
 	uint8_t i;

 	for(i=0;i<16;i++)														// do for all 16 bits in word
 	{
 		if((tempWord&(1<<i))^(PrevWord&(1<<i))) 							// has bit changed?
 				{
 					if(tempWord&(1<<i))										// Is pressed?
 					{
 						if(i == 0)
 						{
 							printf("pedestrian button has been pressed\n");
 							sem_post(&State0Sem);
 							pthread_mutex_lock(&StatemachineProtect);
 							Pedestrian = true;								// pedestrian button has been pressed
 							pthread_mutex_unlock(&StatemachineProtect);

 						}
 						else if(i==12)
 						{
 							printf("train sensor has been set.. Train is on its way!!!\n");
 							sem_post(&State0Sem);
 							pthread_mutex_lock(&StatemachineProtect);
 							Train = true;								// pedestrian button has been pressed
 							pthread_mutex_unlock(&StatemachineProtect);

 						}

 						else if (i==15)
 						{
 							printf("Boomgates are down\n");

 						}

 					}
 					else													// Is released
 					{
 						if(i==12)
 						 {
 							printf("train sensor has been cleared.. Train has passed by!!!\n");
 							pthread_mutex_lock(&StatemachineProtect);
 							Train = false;								// pedestrian button has been pressed
 							pthread_mutex_unlock(&StatemachineProtect);

 						 }

 						 else if (i==15)
 						 {
 							printf("Boomgates are now up\n");

 						 }


 					}
 				}
 	}
 	PrevWord = word;			// copy current word to previous word for next time.
 }
