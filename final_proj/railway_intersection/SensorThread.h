/*
 * SensorThread.h
 *
 *  Created on: 18/05/2017
 *      Author: s9707730
 */

#ifndef SENSORTHREAD_H_
#define SENSORTHREAD_H_

#include <stdlib.h>
#include <stdio.h>
#include "stdbool.h"
#include <fcntl.h>
#include "stdint.h"
#include <hw/inout.h>
#include <pthread.h>
#include "TimerThread.h"

extern int CrossingState;

extern bool Pedestrian, Train, Timer;


 void *SensorThread (void *data);
 void strobe_SCL(uintptr_t gpio_port_add);
 void delaySCL();
 uint32_t KeypadReadIObit(uintptr_t gpio_base, uint32_t BitsToRead);
 void DecodeKeyValue(uint32_t word);

#endif /* SENSORTHREAD_H_ */
